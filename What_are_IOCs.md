# What are IOCs

An Indicator of compromise (IOC) is a piece of forensic data, such as data found in system log entries or files, that identify potentially malicious activity on a system or network.

Examples of an IOC include email addresses with written like a well-known one (examples: opple@support.com, macrosoft@helpdesk.net), the IPs they originate from, and the attachments or links that are in emails. These and pieces of data allow security teams monitoring the systems and networks to spot malicious actors earlier in the intrusion detection process.

In this situation, IOCs are input into a firewall, spam filter, or other defensive technology to protect against threats with that data in its digital identity. Although, there are further uses for this information; analysts can gather, sort, and correlate this information to learn about particular adversaries, regular targets, common exploits, and more to further enhance an entity’s security posture. The more data one has, the stronger the defenses. Many entities have started sharing IOCs that they find.

Documenting IOC and their associated threats allows the industry to share this information and improve incident response and computer forensics. For this reason, groups like MISP, OpenIOC, STIX and TAXII among others are putting effort into standardized IOC documentation and reporting.

**Reference:** https://searchsecurity.techtarget.com/definition/Indicators-of-Compromise-IOC 


