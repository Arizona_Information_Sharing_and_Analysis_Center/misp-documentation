# Splunk Integrations

For organizations that use Splunk, here are the steps for utilizing the app MISP42Splunk.

To provide Splunk with an API key for working with the MISP, a sync user must be created.  
The sync user is who’s authkey will be used as the key Splunk will ask for.

1. While logged into MISP as an administrator, select the following options from the drop-down
menu to add a user.
Administration → Add User

![AddUser](/images/Splunk Integrations/AddUser.png)

2. As shown in the graphic above, ensure the “Set password” box is checked. Select ORGNAME
from the box’s dropdown and the “Sync user” role. Click “Submit.”
3. In the “List Users” page, the newly created sync user will be visible.
The icon of an eye will be clicked to show the authkey, and that will be use to grant Splunk
access to MISP information.

![UsersIndex](/images/Splunk Integrations/UsersIndex.png)

The next steps will be for installing the MISP42Splunk application for Splunk and preparing it for
communication with the local MISP.

1. First users will need to log in to the Splunk server and click the “Splunk Apps” icon.

![InstallMISP42Splunk](/images/Splunk Integrations/InstallMISP42Splunk.png)

2. Next, the search bar in the top left will be used to search for the MISP42Splunk app. Once
found, click the install button (will be replaced by “Open App” button). Splunk will ask you to put in
your splunk.com credentials and require a restart the Splunk server.

![SearchMISP42Splunk](/images/Splunk Integrations/SearchMISP42Splunk.png)

3. Then, return to the home page after installation, and click on the MISP42 application in the
left menu.

![ClickMISP42Splunk](/images/Splunk Integrations/ClickMISP42Splunk.png)

4. Lastly, add the local instance of MISP by clicking “Create New Input”, provide it with a name in
Splunk, add the URL the MISP can be located at, and provide the authkey previously
mentioned to the API Key field.

![AddingLocalInstance](/images/Splunk Integrations/AddingLocalInstance.png)

Search queries are for pulling information and alerts are used to push information to MISP.  
For the documentation on how to format queries and for an original source of installation go to the
following GitHub page:  
Git Hub user remg427’s misp42splunk  
https://github.com/remg427/misp42splunk
