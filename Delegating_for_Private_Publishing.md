# Delegating for Private Publishing

In information sharing, privacy of the reporting organization can be important in such case as:
- a potential victim does not want to be associated to an incident.
- to avoid the relation of an organization with the information shared.

MISP has functionality to delegate the publication and completely remove the binding between
the information shared and its organization. If information should be published without the original entity being tied to it, you can delegate the publication to a different entity. That also means they will take the ownership of the event.

The user needs to have a role with "Delegation access" to delegate an event.

### Prerequisites

The MISP setting allowing delegation must be set to true.

**If you are delegating to the AZ-ISAC hub:**
- All the following instructions can be done on one account with Site admin privileges,
such as those on the default admin user.
- AZ-ISAC needs to be a “local” organization. This setting can be found in the “list
organizations” menu and requires site admin privileges to modify.

**Send a delegation request**
To do so, you first need to put the distribution of the event as "your organization only".

![SettingDistribution1](/images/Delegating for Private Publishing/SettingDistribution1.png)

Otherwise the delegation option will not be available.

![SettingDistribution2](/images/Delegating for Private Publishing/SettingDistribution2.png)

When the "Delegate Publishing" option is clicked, a pop-up will show up:

![ChangePublishingEvent](/images/Delegating for Private Publishing/ChangePublishingEvent.png)

Here the options are
- which organization to delegate the event among all those registered on the server. For this example, Setec Astronomy is asked to publish the event.
- The distribution option to put on the event. The other organization (called "recipient") may choose if desired. For this example, the recipient is requested to share to all communities, but it is only a suggestion, and the recipient will be able to modify the diffusion setting if wanted.

![DesiredDistribution](/images/Delegating for Private Publishing/DesiredDistribution.png)

- Finally, a message to the recipient organization can be written.

![MessageFromRecipient](/images/Delegating for Private Publishing/MessageFromRecipient.png)

Once the request is sent, a message will appear on the event to show the request.

![MessageAppear](/images/Delegating for Private Publishing/MessageAppear.png)

More details can be seen by clicking on "View request details"

![ViewRequestDetails](/images/Delegating for Private Publishing/ViewRequestDetails.png)

Lastly, the event can be discarded by using this pop-up or the link in the left menu.
