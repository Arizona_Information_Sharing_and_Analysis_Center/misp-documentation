# MISP Resource Needs

Like with any server, a computer is needed for it to run on.  
Written below are some examples of resource needs for differently focused organizations.  
If none of them work, a calculator for the resource needs of the MISP is in the reference link below.

Disclaimer: These are recommendations from the time of writing. Software resource needs and the
amount of information expected to be shared may change over time.

**Using MISP strictly to share IOCs and utilize them in a firewall:**

(v)CPUs: 1
RAM: 2 GB
Storage: 100 GB (Deletes data when no longer necessary.)

**Using MISP to analyze IOCs (single analyst):**

(v)CPUs: 4
RAM: 8 GB
Storage: 100 GB (Will grow over time)

**Strictly storing IOCs for distribution:**

(v)CPUs: 2
RAM: 2 GB
Storage: 1000 GB

**Reference:** https://www.misp-project.org/MISP-sizer/


