# Cyber Threat Network MISP Installation Procedure

## About this document 
This document describes how to install the Malware Information Sharing Platform (MISP) software and
configure it to connect to the Arizona Information Sharing and Analysis Center’s Cyber Threat Network to share cyber
threat information.

The Arizona Information Sharing and Analysis Center (AZ-ISAC) is a public-private partnership between the Arizona
Counter Terrorism Information Center (ACTIC), Arizona government sharing partners, and Arizona
businesses and organizations.

A key enabler of ACIP’s ability to share cyber threat information is its Cyber Threat Network. The
purpose of the Cyber Threat Network is to minimize Arizona’s cyber risk and improve the community’s
cybersecurity posture through cyber information collection, analysis, and sharing. AZ-ISAC’s Cyber Threat
Network is built on the Malware Information Sharing Platform (MISP), a widely-respected, open-source
threat sharing platform.
##### This document contains

- About Misp
- Installing MISP
- Configuring MISP
- Troubleshoot, and References.

##### Disclaimer
The Arizona Counter Terrorism Information Center (ACTIC) and the Arizona Information Sharing and Analysis Center (AZ-ISAC) are
providing this installation guide in good faith. Analysts have tested and used the instructions in the
guide, yet there are always risks when implementing new systems. Neither the ACTIC, AZ-ISAC, nor its
partners are responsible for issues you may encounter.

## About MISP

The Malware Information Sharing Platform (MISP) is a free and open-source threat intelligence platform
used to share cyber-related indicators-of-compromise (IOC). Originally developed by the Belgian
military, it was adopted by NATO in 2012 and is now used by more than 6,000 organizations worldwide,
including computer emergency response teams like CERT-EU, the Computer Incident Response Center
Luxembourg (CIRCL), and many others.

MISP may be used to gather, share, store, and correlate indicators of compromise of targeted attacks,
threat intelligence, financial fraud information, vulnerability information, and other types of threat
information.

An indicator of compromise (IOC) is an artifact of a potentially malicious event. By being aware of IOCs,
organizations can better detect and prevent cybersecurity incidents. Examples of indicators of
compromise include

- IP addresses of attacking domains or malicious websites 
- Malware hashes
- Senders and/or subject lines of phishing emails, and
- URLs or domain names of botnet command and control servers.

Some security tools and software can integrate with MISP, and the ACIP team may be able to provide
scripts to help with integration. Contact the ACIP team for more information.  
 **Reference:** See “References” below for the ACIP team’s email address.

##### Requirements
MISP has many installation methods. Most are community developed, and as a result, have inconsistent support. This document focuses on the developer-supported installation script for Ubuntu. These instructions, official installation scripts, and the developer testing use Ubuntu Linux.

Since MISP is written in PHP, any Linux Apache MySQL PHP (LAMP) or similar type of system can theoretically install MISP.  
**Reference:** See “References” below for links to documentation detailing system requirements,
compatibilities, and server capacity needs.


## Installing MISP
The table below describes the MISP installation procedure.   
**Reference:** See “References” below for links to documentation on how to install Ubuntu and for installation guides for other operating systems.  
If Ubuntu is not your choice method of installation, please see the reference to “Downloads”

1. **Run the following command to download the MISP installation script.**
```sh
wget -O /tmp/INSTALL.sh https://raw.githubusercontent.com/MISP/MISP/2.4/INSTALL/INSTALL.sh 
```
##### Notes:
- wget downloads the installation script in the link
- The flag, -O tells wget where to put the install script
>*The file is put in /tmp, by default a temporary filesystem, meaning it is in RAM and will be lost upon reboot or loss of power*

2. **Run the following command to execute the previously downloaded installation script with the -c and -M flags:**

```sh
 bash /tmp/INSTALL.sh -c -M
```
##### Notes:
- -c installs the core MISP software
- -M installs a set of bundled, frequently used MISP modules, such as module to import .CSV files
>MISP will successfully install without this flag, but most users will probably want the modules

![coreMISPsoftware](/images/MISP_Installation_Documentation/coreMISPsoftware.png)

3. **If needed, enter “y” to allow the installation script to create the MISP user account.**

![createMISPuseraccount](/images/MISP_Installation_Documentation/createMISPuseraccount.png)

4. **Store and change the MySQL database Admin (root) and User (misp) credentials if needed for future use. The script also stores these credentials in a folder located in the same directory as the installer.**

5. **When prompted, enter “y” to change the MISP_BASEURL.**
##### Notes:
- This may not be in the install script, anymore. If it is not, skip this step.
- The MISP install script gives you the option not to change the MISP_BASEURL, but experience has shown that not changing it often causes issues.
- The MISP_BASEURL is the domain for the server, such as https://sub.domain.gov. If the machine does not have a domain yet, just use https://localhost.

6. **Allow the installation script to continue running.**  
**Important:** Do not run the installation script twice. The script was made to run on a machine only once and to completion. If the script is run a second time, it will very likely break the installation.

##### Notes:
- The rest of the script may take a significant time to run.
- Make sure the computer does not lose internet connectivity, as lost connectivity may cause several installation issues. For example, do not close the laptop lid or allow the computer to “fall asleep” from being idle too long, as that often closes the internet connection.
- Another possible point of failure is missing the time window to type in the admin password. (When you run a command using sudo, the operating system allows you to run another command as sudo shortly after the first without having to reenter the password. So, the possible point of failure may be caused by the script no longer running as sudo.)

**Reference:** See “Troubleshooting” below for things to try if the installation fails.

## Configuring MISP
**This section describes how to configure your MISP instance.**

##### Initial Login
The table below describes the initial login procedure.
1. Open a browser and go to the previously input MISP_BASEURL or the IP of the server.
2. When prompted, enter the username and password:
```sh
Login email: admin@admin.test
Password: admin
```
##### Notes:
- admin@admin.test is the MISP superuser/superadmin account. The ACIP team recommends making a regular admin account to perform basic administrative functions.
- On first login, you must change the superadmin password when prompted.
- You can change the default email address of any account, including superadmin, after you log in.
 

##### Server Hardening Recommendations
Some MISP installation documents recommend hardening the MISP server, and listed below are a couple recommendations. These recommendations are not meant to add, remove, or modify functionality of MISP itself.  
**Reference:** See “References” below for links to documentation on how to implement these recommendations and other hardening documents.

**Apache web server**
>Implement the best configurations possible for the environment.
**Reminder:** It’s always important to harden your servers.

**Brute-force defense
(fail2ban)**
>Implement “fail2ban” to help deter brute-force attacks for servers that
have remote logins, such as SSH.  
**Reference:** See below for installation instructions.

##### Fail2ban Installation
The table below describes the installation procedure for the “fail2ban” tool to help deter brute-force attacks.
1. Run the following command to execute the install command for Ubuntu.
```sh
sudo apt install fail2ban
```
2. Run the following command to configure local daemons.
```sh
sudo cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
```
3. Run the following command to implement local usage configurations.
```sh
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```
4. Run the following command to start the fail2ban service daemon.
```sh
systemctl start fail2ban
```
5. Run the following command to automatically activate fail2ban during startup.
```sh
systemctl enable fail2ban
```
##### CISA Compliance Configurations
For compliance with the Cyber Intelligence Sharing and Protection Act of 2015 (CISA), MISP data must be tagged with its sensitivity classification using the Traffic Light Protocol.  
**Note:** Unless otherwise marked, any cyber threat information shared as part of Arizona CyberInformation Program’s Cyber Threat Network is classified TLP:GREEN.  
**Reference:** See “References” below for links to information on AArizona Information Sharing and Analysis Center governance, the Cyber Intelligence Sharing and Protection Act of 2015, and the Traffic Light Protocol.

To require information tagging and ensure users tag with a standard format, MISP developers included
“taxonomies.” MISP also provides options to add classification tags to individual events, as well as
create tag collections, a preset list of tags to help apply one or multiple tags with fewer clicks.

Steps 1–3 in the procedure below describe how to locate, enable taxonomies, and make TLP:GREEN the
default classification tag. Steps 4–8 describe how to configure MISP to automatically tag new events
TLP:GREEN. Users will have the ability to change an event’s TLP classification, but a TLP tag will be
required before an event can be published.

1. While logged into MISP as an administrator, select the following options from the drop-down menu to locate the “Taxonomy” menu.
_Event Actions → List Taxonomies_

![locateTaxonomy](/images/MISP_Installation_Documentation/locateTaxonomy.png)

2. On the row, TLP (tlp):
```sh
- Check the “Required” box
- Click the plus sign (+) in the “Actions” column
```
3. Click “enable all” after the taxonomy to provide users with the standard format tags.
4. Select the following options from the drop-down
menu to locate the “List Tag Collections” menu
and select “Add Tag Collection.”
_Event Actions → List Taxonomies → Add Tag Collection_  
**Note:** Adding a tag and adding a tag collection are
different. Tag collections are a preset list of tags
to help apply one or multiple tags with fewer
clicks.
![ListTagCollection](/images/MISP_Installation_Documentation/ListTagCollection.png)

5. Give the tag collection a sensible name and
description, ensure the tag is visable to all
organizations, and click “Submit.”
![TagCollectionSensibleName](/images/MISP_Installation_Documentation/TagCollectionSensibleName.png)


6. Once the collection has been added, click on “List Tag Collections” to designate the tags that are applied when your collection is selected. Select the global tags icon (the planet with a “+” icon). Click “Taxonomy Library:tlp.” Click on the text box and either type “tlp:green” or select it from a dropdown that appears when typing. Click “Submit.”
![TaxonomyLibrary-tlp](/images/MISP_Installation_Documentation/TaxonomyLibrary-tlp.png)

7. Navagate to the “Server Settings & Maintenance” menu. Select the “MISP Settings” tab.
_Administration → Server Settings & Maintenance → MISP Settings_
![NavigateMISPsetting](/images/MISP_Installation_Documentation/NavigateMISPsetting.png)

8. Find and double-click on the “MISP.default_event_tag_collection” setting. Select the newly created tag collection from the dropdown menu.
![MISP.default_event_tag_collection](/images/MISP_Installation_Documentation/MISP.default_event_tag_collection.png)

##### Connecting External Entities to Local MISP Instances
This topic describes how to connect external entities to your local MISP instances. To initiate the connection, the admin must create a “sync user.” One suggested technique is to create a different sync
user for each external entity. By using different sync users, administrators can change the keys of one entity when needed without disabling everyone using the same, shared keys.   
**Note:** Only the external (destination) entity needs to complete Steps 1-4 to allow other entities to connect to it. If you want to connect your internal MISP instance to the AZ-ISAC hub (or another external instance), start with Step 5 below.

1. While logged into MISP as an administrator, select the following options from the drop- down menu to add a user.
_Administration → Add User_
![AddUserFromAdmin](/images/MISP_Installation_Documentation/AddUserFromAdmin.png)

2. As shown in the graphic above, ensure the “Set password” box is checked. Select ORGNAME from the box’s dropdown and the “Sync user” role. Click “Submit.”
3. Login as the Sync User. Select the following options from the drop-down menu to locate the “Create Sync Config” option.
_Sync Actions → Create Sync Config_
![ServerConfiguration](/images/MISP_Installation_Documentation/ServerConfiguration.png)

4. Copy the entire sync config in the grey box and send it to the external entity that is going to connect through this account.  
**Note:** The AZ-ISAC team prefers this configuration method. If the admin prefers to be sent the variable data rather than the sync config, that is also an option.
5. To connect internal MISP instances to external MISP entities, contact the external entity and request a sync config.
**Example:** To connect your MISP instance to the AZ-ISAC hub, contact AZ-ISAC@azdps.gov to request a sync config.
6. With the external entity’s sync config and while logged into MISP as an administrator, select
the following options from the drop-down menu. Enter the sync config into the “Paste server data” box and click “Add.”
_Sync Actions → Import Server Settings_
![ImportServerSetting](/images/MISP_Installation_Documentation/ImportServerSetting.png)

7. Ensure your network — specifically your firewall — is configured appropriately to share data.
**Example:** The HTTPS port must be accessible to external devices for the local server’s synchronization function to allow bi-directional information sharing.

#### Troubleshooting
Listed below are some possible fixes to potential issues. For more information, see “References” below.

##### Declined inputting MISP_BASEURL during install
```sh
sudo -u www-data vim /var/www/MISP/app/Config/config.php
```
_^ Change the “baseurl” variable to match the device’s IP or domain name and restart the machine._
![ChangeBaseURLvariable](/images/MISP_Installation_Documentation/ChangeBaseURLvariable.png)

##### Web interface redirects me to an HTTPS site, but it is blank
```sh
An issue likely occurred during installation. Change the “baseurl” variable as described above. If that fails, reimage the machine and retry installation.
```
##### The page “Create Sync Config” says “[Type] accounts cannot be used to create server sync configurations”

```sh
This is most commonly found when an admin is logged in and goes to the “Create Sync Config” tab.
```
```sh
Login as a sync user to view the sync config.
```
```sh
If you are logged in as a sync user, the user’s role may be misconfigured or the sync role may have been reconfigured.
```
```sh
Check the “List Roles” (Administration → List Roles) settings to ensure each role that needs “sync actions” permission has it.
```
**Note:** You must be an administrator to manage permissions.

#### References
AZ-ISAC website to distribute information about MISP
>https://sites.google.com/a/azdoa.gov/threat-sharing/home

Arizona Information Sharing and Analysis Center (AZ-ISAC) Cyber
Threat Network contact for access, questions, concerns
> AZ-ISAC@azdps.gov

Arizona Information Sharing and Analysis Center governance,
including a program overview, roles and
responsibilities, definitions, and additional
information
>Cyber Threat Network Governance
Contact AZ-ISAC@azdps.gov for a copy.

Cyber Intelligence Sharing and Protection Act of
2015
>https://www.congress.gov/bill/114th-congress/house-bill/234

Hardening — Apache web server
>https://ssl-config.mozilla.org/#server=apache

Hardening — fail2ban tool’s basic usage guide
>https://linuxhandbook.com/fail2ban-basic/

Hardening – MISP, which includes device
hardening considerations and questions to
consider for MISP
>https://misp.github.io/MISP/generic/hardening/

MISP configuration tool, which provides a rough
estimate of RAM, CPU, and disk space needs of
the server
>https://www.misp-project.org/MISP-sizer/

MISP documentation list
>https://www.misp-project.org/documentation/

MISP FAQ and troubleshooting
>https://www.circl.lu/doc/misp/faq/

MISP history
>https://www.misp-project.org/who/

MISP installation guides for various operating
systems
##### Notes:
- The Ubuntu sections of the MISP GitHub page
provide commands to use their automated
installation script.
- CentOS/RHEL sections do not currently have
an installation script. If you’re using these
operating systems, see Ubuntu installation
documents below.
- The “xinstall” guides may or may not be
tested.

>https://misp.github.io/MISP/
- The site, misp.github.io/MISP/ is not always fully up-to-date 

>https://github.com/misp/misp

- The most current documentation is on the
MISP GitHub page, github.com/misp/misp, in the “docs” folder.

> **Notes:**
- The Ubuntu sections of the MISP GitHub page
provide commands to use their automated
installation script.
- CentOS/RHEL sections do not currently have
an installation script. If you’re using these
operating systems, see Ubuntu installation
documents below.
- The “xinstall” guides may or may not be
tested.

The most current documentation is on the MISP GitHub page, github.com/misp/misp, in the “docs” folder.
>https://www.circl.lu/doc/misp/requirements/

MISP searchable documentation
>https://www.circl.lu/doc/misp/

Traffic Light Protocol for classifying information according to its sensitivity
>https://www.us-cert.gov/tlp

Travis automated testing pipeline to see what is used for testing MISP software
>https://travis-ci.org/github/MISP/MISP

Ubuntu installation documents

Note: This link also includes the MISP installation
guide for CentOS or RHEL.
>https://help.ubuntu.com/community/Installation

“Download” page for various version of MISP
(i.e. Container, Ansible, Virtual Images, etc...)
>https://www.misp-project.org/download/
