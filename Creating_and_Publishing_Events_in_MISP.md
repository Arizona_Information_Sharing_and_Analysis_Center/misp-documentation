# Creating and Publishing Events in MISP
## Creating the Event

The process of entering an event can be split into 3 phases, the creation of the event itself, populating it
with attributes and attachments and finally publishing it.

During this first step, you will create a basic event without any actual attributes, but storing general
information such as a description, time and risk level of the incident. To start creating the event, click on
the New Event button on the left.

![EventScreenshot](/images/Creating and Publishing Events in MISP/EventScreenshot.png)

Then, fill out the form you are presented with. The following fields need to be filled out:

![AddEvent](/images/Creating and Publishing Events in MISP/AddEvent.png)

##### Threat
The current definitions AZ-ISAC is working with to determine the threat level of a given event.
- **High:** Sophisticated APTs and 0day attacks.
- **Medium:** Advanced Persistent Threats (APT)
- **Low:** General mass malware

##### Analysis
Indicates the current stage of the analysis for the event, with the following possible options:
- **Initial:** The analysis is just beginning
- **Ongoing:** The analysis is in progress
- **Completed:** The analysis is complete

##### Distribution
The distribution is the most complex to understand for most users. The distribution simply describes
who will be able to receive the IOCs that are being shared when the event gets published including other
users on the same server and other MISP server. The definitions can be confusing, so these have slight
modifications in an attempt to make it clearer. For the developer’s definitions, read the reference on
creating MISP events.
- **Your organization only:** This setting will only allow members of your organisation to see this. It can be pulled to another instance by one of your organisation members where only your organisation will be able to see it. Events with this setting will not be synchronized with other MISP servers. When a permitted user pulls this event, this event gets pulled.
- **This Community-only:** Users that are part of your MISP community will be able to see the event. This includes your own organisation, organisations on this MISP server and organisations running MISP servers that synchronise with this server. Any other organisations connected to such linked servers will be restricted from seeing the event. When this server pushes events, this event doesn’t push. When events are pulled, this event gets pulled. The distribution of this
event on the pulling MISP becomes Your Organization Only.
- **Connected communities:** Users that are part of your MISP community will be able to see the event. This includes all organisations on this MISP server, all organisations on MISP servers synchronising with this server and the hosting organisations of servers that connect to those afore mentioned servers (so basically any server that is 2 hops away from this one). Any other organisations connected to linked servers that are 2 hops away from this own will be restricted from seeing the event. When this event is pushed, downgrade distribution to This Community only and push to other servers. When this event is pulled, it gets pulled and downgrade to This Community only.
- **All communities:** This will share the event with all MISP communities, allowing the event to be freely propagated from one server to the next. This distribution does not get downgraded the more it is pushed or pulled. When pushed, the event is push. When pulled, the event is pulled.
- **Sharing group:** This will share the event to the defined sharing group. This includes only the organisations defined in the sharing group. The distribution can be local and cross-instance depending of the sharing group definition. For more information on sharing groups, refer to the sharing group section.

## Adding Attributes
Attributes are the IOCs of a given event. These can be added in various ways. More can be added by importing them as a MISP module, such as CSV imports. 

One of the most commonly used importing utilities is the Free-Text Import Tool. It will locate IOCs in a large block of text that gets pasted into it. The tool is imperfect, as it will disregard some information, such as email subjects (not necessarily identifiable where they start or end in a block of text) but does well at getting most of what is important.

On the left-hand column, click the button that says, “Populate from”.

![Feetext_Import_Tool](/images/Creating and Publishing Events in MISP/Freetext_Import_Tool.jpg)

This screen will show for reviewing what it has located:

![Freetext_Import_Results](/images/Creating and Publishing Events in MISP/Freetext_Import_Results.jpg)

Since there are several category / type combinations that can be valid for a lot of values, MISP will suggest the most common settings. You can alter the category / type / IDS fields manually if you disagree with the results. The options will be restricted to valid category/type combinations for the value that you have entered.

If any correlation is already found, these correlations will be displayed in the result page.

## Publishing Events
Lastly, to share events with others, in the event that will be published, click on the Publish Event button on the left-hand column.

![Initial_Event](/images/Creating and Publishing Events in MISP/Initial_Event.jpg)

This will alert the eligible users of it (based on the private-controls of the event and its attributes/attachments and whether they have auto-alert turned on), push the event to instances that your instance connects to and propagate it further based on the distribution rules.

If you’d like to share anonymously, please read the “Delegation” training documents.

**Event Creation Reference:** https://www.circl.lu/doc/misp/using-the-system/#creating-an-event
**Publishing reference:** https://www.circl.lu/doc/misp/using-the-system/#publish-an-event
