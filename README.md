# MISP Documentation



## Getting started

The following content is a guide to help your organization create a MISP. It contains steps for understanding how to use and protect MISP.

- [MISP Resource Needs](MISP_Resource_Needs.md)
- [MISP Installation Documentation](MISP_Installation_Documentation.md)
- [What are IOCs](What_are_IOCs.md)
- [Creating and Publishing Events in MISP](Creating_and_Publishing_Events_in_MISP.md)
- [Pulling and Pushing IOCs in MISP](Pulling_and_Pushing_IOCs_in_MISP.md)
- [Delegating for Private Publishing](Delegating_for_Private_Publishing.md)
- [Splunk Integrations](Splunk_Integrations.md)

Please refer to the reference sections of each document if you are confused. They may prove useful.

## MISP Resource Needs

A document providing suggestions and resources for determining how many resource will be needed to run the vitural machine.

## MISP Installation Documentation

This document describes how to install the Malware Information Sharing Platform (MISP) and configure it to connect to the Arizona Information Sharing and Analysis Center to share cyber threat information.

## What are IOCs
Describes what an IOC is.

## Creating and Publishing Events in MISP

The process for entering an event.
- Create the event
- Add attributes
- Publish

## Pulling and Pushing IOCs in MISP
This documentation is intended for use after a server has been added for synchronization.  
**Added a server, such as the ACIP Hub, before following these instructions.**

# Delegating for Private Publishing

MISP has functionality to delegate the publication and completely remove the binding between the information shared and its organization. 

## Splunk Integrations
For organizations that use Splunk, here are the steps for utilizing the app MISP42Splunk...

