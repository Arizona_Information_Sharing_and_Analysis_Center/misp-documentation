# Pulling and Pushing IOCs in MISP

This documentation is intended for use after a server has been added for synchronization. Adding a server for synchronization is the last set of instructions in the MISP installation guide. Added a server, such as the ACIP Hub, before following these instructions.

### Manual Push/Pull of information

Pulling IOCs is a simpler concept and will likely be used more often than pushing IOCs. In order to pull IOCs, the external server that the local instance is pulling from simply needs to be accessible through HTTPS and the local user needs to have the authorization key for the external server to pull information.

Pushing will be more complex. Please read the different distribution settings that events have when they get published either in the reference below or in the training document on publishing IOCs.

1. Test your connection with the server to ensure a pull can occur.
This can be done by going to Sync Actions -> List Servers -> Run.

![TestYourConnection](/images/Pulling and Pushing IOCs in MISP/TestYourConnection.png)

2. On the right side of the above scree, there will be a push and a pull button. (if not, click the pencil to edit the configuration, and make sure push/pull is checked.
To manually push/pull from the external server press the correlating button.

![ManualPullButton](/images/Pulling and Pushing IOCs in MISP/ManualPullButton.png)

### Automated Pulling/Pushing of Information

For entities that would rather not login regularly to obtain or send information, the MISP has a built-in
method to schedule a few tasks. These tasks are not the limit of what a MISP can do, but they are what
is built-in to MISP.
- Fetch threat feeds
- Cache threat feeds
- Push all published IOCs
- Pull all available IOCs
- Cache Exports

Most use cases will likely utilize the automated pulling feature. So that will be focused on for this
documentation.
1. Go to the Scheduled Tasks page by clicking
Administration -> Scheduled Tasks

    ![GoToScheduledTasks](/images/Pulling and Pushing IOCs in MISP/GoToScheduledTasks.png)

2. The following is the Scheduled Tasks screen.

    ![LookScheduledTasks](/images/Pulling and Pushing IOCs in MISP/LookScheduledTasks.png)

    There are two settings of importance. “Next Run” and “Frequency”.
    To edit either of their values, double click them.

    Frequency is how regularly it attempts to pull new information from all the servers it is connected to. The value is based on hours, thus a value of 1 means the server pulls from all connected servers every hour.

    The “Next Run” is the date the next execution of the process will occur. If the setting is at a date earlier than the current day, the schedule will be considered disabled, unless it started running from something it was set to previously. Thus, to disable the schedule, change the “next run” to a date before the current day.


3. Now that they are all set, just wait for the time to come.

    One can check if it ran by going into
    Server System and maintenance -> Jobs.

    In this menu, the executed job will be displayed, showing if queued, in progress, completed, or failed.
    Assuming nobody ran the command manually, if the job for the scheduled task shows, then the schedule works.

**Distribution Values Reference:** https://www.circl.lu/doc/misp/using-the-system/#creating-an-event

**Reference:** https://www.circl.lu/doc/misp/using-the-system/#browsing-the-currently-set-up-server-connections-and-interacting-with-them


---------
